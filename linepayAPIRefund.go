package linepay

import (
	"context"
	"fmt"
	"net/http"
	"time"
)

// Refund API
// 本 API 用以取消已付款(購買完成)的交易，並可支援部分退款。呼叫時需要帶入該筆付款的 LINE Pay 原始交易序號(transactionId)
func (c *Client) Refund(ctx context.Context, transactionID int64, req *RefundRequest) (*RefundResponse, *http.Response, error) {
	path := fmt.Sprintf("/v3/payments/%d/refund", transactionID)
	httpReq, err := c.NewRequest(http.MethodPost, path, req)
	if err != nil {
		return nil, nil, err
	}
	resp := new(RefundResponse)
	httpResp, err := c.Do(ctx, httpReq, resp)
	if err != nil {
		return nil, httpResp, err
	}
	return resp, httpResp, nil
}

// RefundRequest type
type RefundRequest struct {
	RefundAmount int `json:"refundAmount,omitempty"` // 退款金額 (返回空值的話，進行全部退款)
	Options      struct {
		Extra struct {
			PromotionRestriction map[string]int `json:"promotionRestriction,omitempty"` // 點數限制資訊 useLimit: 不可使用點數折抵的金額 rewardLimit: 不可回饋點數的金額
			/* Example of promotionRestriction
			"promotionRestriction": {
					"useLimit": 100,
					"rewardLimit": 100
			}
			*/
		} `json:"extra,omitempty"`
	} `json:"options,omitempty"`
}

// RefundResponse type
type RefundResponse struct {
	ReturnCode    string `json:"returnCode"`    // 結果代碼
	ReturnMessage string `json:"returnMessage"` // 結果訊息或失敗原因
	Info          struct {
		RefundTransactionID   int64     `json:"refundTransactionId"`   // 退款序號（該次退款產生的新序號, 19 digits）
		RefundTransactionDate time.Time `json:"refundTransactionDate"` // 退款日期（ISO 8601）
	} `json:"info"`
}

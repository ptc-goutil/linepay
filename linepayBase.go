package linepay

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/url"
	"reflect"
	"strings"

	"github.com/google/go-querystring/query"
	"github.com/google/uuid"
)

// API endpoint base constants
const (
	APIEndpointReal    = "https://api-pay.line.me"
	APIEndpointSandbox = "https://sandbox-api-pay.line.me"
)

// Client type
type Client struct {
	channelID     string
	channelSecret string
	endpoint      *url.URL
	httpClient    *http.Client
}

// ClientOption type
type ClientOption func(*Client) error

// New returns a new pay client instance.
func New(channelID, channelSecret string, options ...ClientOption) (*Client, error) {
	if channelID == "" {
		return nil, errors.New("missing channel id")
	}
	if channelSecret == "" {
		return nil, errors.New("missing channel secret")
	}
	c := &Client{
		channelID:     channelID,
		channelSecret: channelSecret,
		httpClient:    http.DefaultClient,
	}
	for _, option := range options {
		err := option(c)
		if err != nil {
			return nil, err
		}
	}
	if c.endpoint == nil {
		u, err := url.Parse(APIEndpointSandbox)
		if err != nil {
			return nil, err
		}
		c.endpoint = u
	}
	return c, nil
}

func NewClientByParam(channelID, channelSecret string, endpoint *url.URL, httpClient *http.Client) *Client {
	return &Client{
		channelID:     channelID,
		channelSecret: channelSecret,
		endpoint:      endpoint,
		httpClient:    httpClient,
	}
}

// WithHTTPClient function
func WithHTTPClient(c *http.Client) ClientOption {
	return func(client *Client) error {
		client.httpClient = c
		return nil
	}
}

// WithEndpoint function
func WithEndpoint(endpoint string) ClientOption {
	return func(client *Client) error {
		u, err := url.Parse(endpoint)
		if err != nil {
			return err
		}
		client.endpoint = u
		return nil
	}
}

// WithSandbox function
func WithSandbox() ClientOption {
	return WithEndpoint(APIEndpointSandbox)
}

// mergeQuery method
func (c *Client) mergeQuery(path string, q interface{}) (string, error) {
	v := reflect.ValueOf(q)
	if v.Kind() == reflect.Ptr && v.IsNil() {
		return path, nil
	}

	u, err := url.Parse(path)
	if err != nil {
		return path, err
	}

	qs, err := query.Values(q)
	if err != nil {
		return path, err
	}

	u.RawQuery = qs.Encode()
	return u.String(), nil
}

// NewRequest method
func (c *Client) NewRequest(method, path string, body interface{}) (*http.Request, error) {
	var builder strings.Builder
	builder.WriteString(c.channelSecret)
	builder.WriteString(path)

	switch method {
	case http.MethodGet, http.MethodDelete:
		if body != nil {
			merged, err := c.mergeQuery(path, body)
			if err != nil {
				return nil, err
			}
			path = merged
		}
	}
	u, err := c.endpoint.Parse(path)
	if err != nil {
		return nil, err
	}

	var reqBody io.ReadWriter
	switch method {
	case http.MethodGet, http.MethodDelete:
		if body != nil {
			builder.WriteString(u.RawQuery)
		}
	case http.MethodPost, http.MethodPut:
		if body != nil {
			b, err := json.Marshal(body)
			if err != nil {
				return nil, err
			}
			reqBody = bytes.NewBuffer(b)
			builder.WriteString(string(b))
		}
	}

	nounce := uuid.New().String()
	builder.WriteString(nounce)

	req, err := http.NewRequest(method, u.String(), reqBody)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-LINE-ChannelId", c.channelID)
	req.Header.Set("X-LINE-Authorization-Nonce", nounce)

	hash := hmac.New(sha256.New, []byte(c.channelSecret))
	hash.Write([]byte(builder.String()))
	req.Header.Set("X-LINE-Authorization", base64.StdEncoding.EncodeToString(hash.Sum(nil)))
	return req, nil
}

// Do method
func (c *Client) Do(ctx context.Context, req *http.Request, v interface{}) (*http.Response, error) {
	resp, err := c.httpClient.Do(req.WithContext(ctx))
	if err != nil {
		if ctx.Err() != nil {
			return nil, ctx.Err()
		}
		return nil, err
	}
	defer resp.Body.Close()

	if v != nil {
		switch v := v.(type) {
		case io.Writer:
			_, err = io.Copy(v, resp.Body)
		default:
			err = json.NewDecoder(resp.Body).Decode(v)
		}
		if err != nil {
			return resp, err
		}
	}
	return resp, nil
}

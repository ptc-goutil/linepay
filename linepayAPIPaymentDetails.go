package linepay

import (
	"context"
	"net/http"
	"time"
)

// PaymentDetails API
// 本API查詢LINE Pay中的交易記錄。您可以查詢授權和購買完成狀態的交易。使用"fields"設定，可以按交易或訂單資訊，選擇查出交易記錄。
// 作為參數值，需要提供一個或一個以上的交易序號或訂單編號。每次單一列表可查詢最多100筆序號或編號。
func (c *Client) PaymentDetails(ctx context.Context, req *PaymentDetailsRequest) (*PaymentDetailsResponse, *http.Response, error) {
	path := "/v3/payments"
	httpReq, err := c.NewRequest(http.MethodGet, path, req)
	if err != nil {
		return nil, nil, err
	}
	resp := new(PaymentDetailsResponse)
	httpResp, err := c.Do(ctx, httpReq, resp)
	if err != nil {
		return nil, httpResp, err
	}
	return resp, httpResp, nil
}

// PaymentDetailsRequest type
type PaymentDetailsRequest struct {
	TransactionID []int64  `url:"transactionId,omitempty"` // 由LINE Pay建立的交易序號或退款序號
	OrderID       []string `url:"orderId,omitempty"`       // 商家訂單編號
	Fields        string   `url:"fields,omitempty"`        // 可以選擇查詢物件 transaction order 預設為所有
}

// PaymentDetailsResponse type
type PaymentDetailsResponse struct {
	ReturnCode    string `json:"returnCode"`    // 結果代碼
	ReturnMessage string `json:"returnMessage"` // 結果訊息或失敗原因

	// **在查詢Transaction類型時回應如下：
	Info []*struct {
		TransactionID           int64     `json:"transactionId"`           // 交易序號（19個字元）
		TransactionDate         time.Time `json:"transactionDate"`         // 交易日期（ISO-8601）
		TransactionType         string    `json:"transactionType"`         // 交易分類 PAYMENT：付款 PAYMENT_REFUND：退款 PARTIAL_REFUND：部分退款
		PayStatus               string    `json:"payStatus"`               // 付款狀態 CAPTURE：已請款 AUTHORIZATION：授權 VOIDED_AUTHORIZATION：授權無效（在呼叫"Void API"的狀態） EXPIRED_AUTHORIZATION：授權到期（LINE Pay所允許的商家授權有效期已過期）
		ProductName             string    `json:"productName"`             // 商品名稱
		MerchantName            string    `json:"merchantName"`            // 商家名稱
		Currency                string    `json:"currency"`                // 貨幣（ISO 4217）
		AuthorizationExpireDate string    `json:"authorizationExpireDate"` // 授權交易到期時間（ISO-8601）
		PayInfo                 []*struct {
			// **使用LINE Pay優惠劵時，會於API Response info.payInfo[].method 中回覆DISCOUNT金額，商品如需開立發票，必須注意發票金額需扣除DISCOUNT(折扣)金額。
			Method string `json:"method"` // 付款方式 信用卡：CREDIT_CARD 餘額：BALANCE 折扣：DISCOUNT (發票金額須扣除) LINE POINTS：POINT (預設不顯示)
			Amount int    `json:"amount"` // Number		交易金額（建立交易序號時提供的金額） 在檢視原始交易時，最終交易金額的演算法如下： sum(info[].payInfo[].amount) – sum(refundList[].refundAmount)
		} `json:"payInfo"`
		MerchantReference struct {
			AffiliateCards []*struct {
				CardType string `json:"cardType"` // 交易中若用戶符合商店支援的卡片類型 - 電子發票載具: MOBILE_CARRIER (功能預設不開啟) - 商家會員卡: {類別名稱需與LINE Pay洽談確認}
				CardId   string `json:"cardId"`   // 交易中若用戶符合商店支援的卡片類型所對應的內容值
			} `json:"affiliateCards"`
		} `json:"merchantReference"`
		// **在查詢Transaction類型時，對原始交易與退款交易的回應如下：
		RefundList []*struct {
			RefundTransactionID   string    `json:"refundTransactionId"`   // 退款序號（19個字元）
			TransactionType       string    `json:"transactionType"`       // 交易分類 PAYMENT_REFUND：退款 PARTIAL_REFUND：部分退款
			RefundAmount          int       `json:"refundAmount"`          // 退款
			RefundTransactionDate time.Time `json:"refundTransactionDate"` // 退款日期 (ISO-8601)
		} `json:"refundList,omitempty"`

		// **在查詢Transaction類型時，對退款交易的應如下：
		OriginalTransactionID int64 `json:"originalTransactionId,omitempty"` // 原始交易序號（19個字元）
	} `json:"info"`
	// **在查詢Order類型時回應如下：
	Packages []*struct {
		ID            string `json:"id"`            // Package list的唯一ID
		Amount        int    `json:"amount"`        // 一個Package中的商品總價 =sum(products[].quantity * products[].price)
		UserFeeAmount int    `json:"userFeeAmount"` // 手續費：在付款金額中含手續費時回應
		Name          string `json:"name"`          // 配送單位 or （內部Shop Name）
		Products      []*struct {
			ID            string `json:"id"`            // 商家商品ID
			Name          string `json:"name"`          // Required: 商品名
			ImageURL      string `json:"imageUrl"`      // 商品圖示的URL
			Quantity      int    `json:"quantity"`      // Required: 商品數量
			Price         int    `json:"price"`         // Required: 各商品付款金額
			OriginalPrice int    `json:"originalPrice"` // 各商品原價
		} `json:"products"`
	} `json:"packages"`
	MerchantReference struct {
		AffiliateCards []*struct {
			CardType string `json:"cardType"` // 交易中若用戶符合商店支援的卡片類型 - 電子發票載具: MOBILE_CARRIER (功能預設不開啟) - 商家會員卡: {類別名稱需與LINE Pay洽談確認}
			CardId   string `json:"cardId"`   // 交易中若用戶符合商店支援的卡片類型所對應的內容值
		} `json:"affiliateCards"`
	} `json:"merchantReference"`
	// info.shipping為使用到Checkout時的資訊，目前Checkout只限定日本可以使用
	Shipping *struct {
		MethondId string `json:"methodId,omitempty"`  // 用戶所選的配送方式ID
		FeeAmount int    `json:"feeAmount,omitempty"` // 運費
		Address   *struct {
			Country    string `json:"country,omitempty"`    // 收貨國家
			PostalCode string `json:"postalCode,omitempty"` // 收貨地郵政編碼
			State      string `json:"state,omitempty"`      // 收貨地區
			City       string `json:"city,omitempty"`       // 收貨省市區
			Detail     string `json:"detail,omitempty"`     // 收貨地址
			Optional   string `json:"optional,omitempty"`   // 詳細地址資訊
			Recipient  *struct {
				FirstName         string `json:"firstName,omitempty"`         // 收貨人名
				LastName          string `json:"lastName,omitempty"`          // 收貨人姓
				FirstNameOptional string `json:"firstNameOptional,omitempty"` // 詳細名資訊
				LastNameOptional  string `json:"lastNameOptional,omitempty"`  // 詳細姓資訊
				Email             string `json:"email,omitempty"`             // 收貨人電子郵件
				PhoneNo           string `json:"phoneNo,omitempty"`           // 收貨人電話號碼
			} `json:"recipient,omitempty"`
		} `json:"address,omitempty"`
	} `json:"shipping,omitempty"`
	// 在查詢events類型時回應如下：
	Events []*struct {
		Code            string `json:"code"`            // 指定品項組合所符合的代碼
		TotalAmount     int    `json:"totalAmount"`     // 指定品項組合所符合的銷售金額。
		ProductQuantity int    `json:"productQuantity"` // 指定品項組合所符合的組合數量。
	} `json:"events"`
}

package linepay

import (
	"context"
	"net/http"
)

// Request API
// 本API向LINE Pay請求付款資訊。由此，可以設定使用者的交易資訊與付款方式。請求成功，將生成LINE Pay交易序號，可以進行付款與退款。
func (c *Client) Request(ctx context.Context, req *RequestRequest) (*RequestResponse, *http.Response, error) {
	path := "/v3/payments/request"
	httpReq, err := c.NewRequest(http.MethodPost, path, req)
	if err != nil {
		return nil, nil, err
	}
	resp := new(RequestResponse)
	httpResp, err := c.Do(ctx, httpReq, resp)
	if err != nil {
		return nil, httpResp, err
	}
	return resp, httpResp, nil
}

// RequestRequest type
// 無 omitempty 則為必填
type RequestRequest struct {
	Amount   int    `json:"amount"`   // Required: 付款金額 = sum(packages[].amount) + sum(packages[].userFee) + options.shipping.feeAmount
	Currency string `json:"currency"` // Required: 貨幣（ISO 4217）支援貨幣：USD、JPY、TWD、THB
	OrderID  string `json:"orderId"`  // Required: 商家訂單編號，由商家管理的唯一ID
	Packages []*struct {
		ID       string `json:"id"`                // Required: Package list的唯一ID
		Amount   int    `json:"amount"`            // Required: 一個Package中的商品總價 = sum(products[].quantity * products[].price)
		UserFee  int    `json:"userFee,omitempty"` // 手續費：在付款金額中含手續費時設定
		Name     string `json:"name"`              // Required: Package名稱 （or Shop Name）
		Products []*struct {
			ID            string `json:"id,omitempty"`            // 商家商品ID
			Name          string `json:"name"`                    // Required: 商品名
			ImageURL      string `json:"imageUrl,omitempty"`      // 商品圖示的URL
			Quantity      int    `json:"quantity"`                // Required: 商品數量
			Price         int    `json:"price"`                   // Required: 各商品付款金額
			OriginalPrice int    `json:"originalPrice,omitempty"` // 各商品原價
		} `json:"products"`
	} `json:"packages"`
	RedirectURLs *struct {
		AppPackageName string `json:"appPackageName,omitempty"` // 在Android環境切換應用時所需的資訊，用於防止網路釣魚攻擊（phishing）
		ConfirmURL     string `json:"confirmUrl"`               // Required: 使用者授權付款後，跳轉到該商家URL
		ConfirmURLType string `json:"confirmUrlType,omitempty"` // 使用者授權付款後，跳轉的confirmUrl類型
		CancelURL      string `json:"cancelUrl"`                // Required: 使用者通過LINE付款頁，取消付款後跳轉到該URL
	} `json:"redirectUrls"`
	Options *struct {
		Payment *struct {
			Capture *bool  `json:"capture,omitempty"` // 是否自動請款 true(預設)：呼叫Confirm API，統一進行授權/請款處理 false：呼叫Confirm API只能完成授權，需要呼叫Capture API完成請款
			PayType string `json:"payType,omitempty"` // 付款類型 "NORMAL", "PREAPPROVED"
		} `json:"payment,omitempty"`
		Display *struct {
			Locale                 string `json:"locale,omitempty"`                 // 等待付款頁的語言程式碼，預設為英文（en）支援語言：en、ja、ko、th、zh_TW、zh_CN
			CheckConfirmURLBrowser *bool  `json:"checkConfirmUrlBrowser,omitempty"` // 檢查將用於訪問confirmUrl的瀏覽器 true：如果跟請求付款的瀏覽器不同，引導使用LINE Pay請求付款的瀏覽器 false：無需檢查瀏覽器，直接訪問confirmUrl
		} `json:"display,omitempty"`
		Shipping *struct {
			Type           string `json:"type,omitempty"`           // 收貨地選項 "NO_SHIPPING","FIXED_ADDRESS","SHIPPING"
			FeeAmount      string `json:"feeAmount,omitempty"`      // 運費
			FeeInquiryURL  string `json:"feeInquiryUrl,omitempty"`  // 查詢配送方式的URL
			FeeInquiryType string `json:"feeInquiryType,omitempty"` // 運費查詢類型 CONDITION：收貨地發生變化，就查詢配送方式（運費） FIXED：作為固定值，收貨地發生變化，也不會查詢配送方式
			Address        *struct {
				Country    string `json:"country,omitempty"`    // 收貨國家
				PostalCode string `json:"postalCode,omitempty"` // 收貨地郵政編碼
				State      string `json:"state,omitempty"`      // 收貨地區
				City       string `json:"city,omitempty"`       // 收貨省市區
				Detail     string `json:"detail,omitempty"`     // 收貨地址
				Optional   string `json:"optional,omitempty"`   // 詳細地址資訊
				Recipient  *struct {
					FirstName         string `json:"firstName,omitempty"`         // 收貨人名
					LastName          string `json:"lastName,omitempty"`          // 收貨人姓
					FirstNameOptional string `json:"firstNameOptional,omitempty"` // 詳細名資訊
					LastNameOptional  string `json:"lastNameOptional,omitempty"`  // 詳細姓資訊
					Email             string `json:"email,omitempty"`             // 收貨人電子郵件
					PhoneNo           string `json:"phoneNo,omitempty"`           // 收貨人電話號碼
				} `json:"recipient"`
			} `json:"address,omitempty"`
		} `json:"shipping,omitempty"`
		FamilyService *struct {
			AddFriends []*struct {
				Type string   `json:"type,omitempty"` //新增好友的服務類型 "lineAt"
				IDs  []string `json:"ids,omitempty"`  // 各服務類型的ID list
			} `json:"addFriends,omitempty"`
		} `json:"familyService,omitempty"`
		Extra *struct {
			BranchName           string         `json:"branchName,omitempty"`           // 商店或分店名稱(僅會顯示前 100 字元)
			BranchID             string         `json:"branchId,omitempty"`             // 商店或分店代號，可支援英數字及特殊字元
			PromotionRestriction map[string]int `json:"promotionRestriction,omitempty"` // 點數限制資訊 useLimit: 不可使用點數折抵的金額 rewardLimit: 不可回饋點數的金額
			/* Example of promotionRestriction
			"promotionRestriction": {
					"useLimit": 100,
					"rewardLimit": 100
			}
			*/
		} `json:"extra,omitempty"`
	} `json:"options,omitempty"`
}

// RequestResponse type
type RequestResponse struct {
	ReturnCode    string `json:"returnCode"`    // 結果代碼
	ReturnMessage string `json:"returnMessage"` // 結果訊息
	Info          struct {
		TransactionID      int64  `json:"transactionId"`      // 交易序號
		PaymentAccessToken string `json:"paymentAccessToken"` // 該代碼在LINE Pay可以代替掃描器使用
		PaymentURL         struct {
			App string `json:"app"` //	用來跳轉到付款頁的App URL a.在應用程式發起付款請求時使用 b.在從商家應用跳轉到LINE Pay時使用
			Web string `json:"web"` // 用來跳轉到付款頁的Web URL a.在網頁請求付款時使用 b.在跳轉到LINEPay等待付款頁時使用 c.不經參數，直接跳轉到傳來的URL d.在Desktop版，彈窗大小為Width：700px，Height：546px
		} `json:"paymentUrl"`
	} `json:"info"`
}

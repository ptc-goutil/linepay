module gitlab.com/ptc-goutil/linepay

go 1.22.2

require (
	github.com/google/go-querystring v1.1.0
	github.com/google/uuid v1.6.0
)

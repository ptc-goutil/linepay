package linepay

import (
	"context"
	"fmt"
	"net/http"
)

// Confirm API
// 在用戶確認付款後，商家可透過confirmUrl或Check Payment Status API，來完成交易。
// 如果Request API中"options.payment.capture"被設置為false，意味著該交易的授權與請款分開。
// 在此情況下，付款完成後，狀態仍然會保持”待請款（授權）”。因此，需呼叫Capture API進行後續處理，才能完成交易的所有流程。
func (c *Client) Confirm(ctx context.Context, transactionID int64, req *ConfirmRequest) (*ConfirmResponse, *http.Response, error) {
	path := fmt.Sprintf("/v3/payments/%d/confirm", transactionID)
	httpReq, err := c.NewRequest(http.MethodPost, path, req)
	if err != nil {
		return nil, nil, err
	}
	resp := new(ConfirmResponse)
	httpResp, err := c.Do(ctx, httpReq, resp)
	if err != nil {
		return nil, httpResp, err
	}
	return resp, httpResp, nil
}

// ConfirmRequest type
type ConfirmRequest struct {
	Amount   int    `json:"amount"`   // 付款金額
	Currency string `json:"currency"` // 貨幣(ISO 4217) 支援下列貨幣：USD JPY TWD THB
}

// ConfirmResponse type
type ConfirmResponse struct {
	ReturnCode    string `json:"returnCode"`    // 結果代碼
	ReturnMessage string `json:"returnMessage"` // 結果訊息
	Info          *struct {
		OrderID                 string `json:"orderId"`                           // 請求付款時，回應的商家唯一訂單編號
		TransactionID           int64  `json:"transactionId"`                     // 作為請求付款的結果，回應的交易序號（19個字符）
		AuthorizationExpireDate string `json:"authorizationExpireDate,omitempty"` // 授權過期時間（ISO 8601）僅限於完成授權（capture=false）的付款，進行回傳。
		RegKey                  string `json:"regKey,omitempty"`                  // 用於自動付款的密鑰（15個字符）
		PayInfo                 []*struct {
			// **使用LINE Pay優惠劵時，會於API Response info.payInfo[].method 中回覆DISCOUNT金額，商品如需開立發票，必須注意發票金額需扣除DISCOUNT(折扣)金額。
			Method                 string `json:"method"`                 // 付款方式 信用卡：CREDIT_CARD 餘額：BALANCE 折扣：DISCOUNT (發票金額須扣除) LINE POINTS：POINT(預設不顯示)
			Amount                 int    `json:"amount"`                 // 付款金額
			CreditCardNickname     string `json:"creditCardNickname"`     // 用於自動付款的信用卡別名 綁定在LINE Pay的信用卡名。一般在綁定信用卡時設置。如果LINE Pay用戶沒有設置別名，會回傳空字符串。用戶通過LINE Pay可以更改別名，更改內容不會分享到商家。
			CreditCardBrand        string `json:"creditCardBrand"`        // 用於自動付款的信用卡品牌 VISA MASTER AMEX DINERS JCB
			MaskedCreditCardNumber string `json:"maskedCreditCardNumber"` // 被遮罩（Masking）的信用卡號（僅限於台灣商家回應，若您需要，可以向商家中心管理者申請獲取。） Format: **** **** **** 1234
		} `json:"payInfo"`
		Packages []*struct {
			ID            string `json:"id"`            // Package list的唯一ID
			Amount        int    `json:"amount"`        // 一個Package中的商品總價 = sum(products[].quantity * products[].price)
			UserFeeAmount int    `json:"userFeeAmount"` // 手續費：在付款金額中含手續費時回應
		} `json:"packages"`
		MerchantReference struct {
			AffiliateCards []*struct {
				CardType string `json:"cardType"` // 交易中若用戶符合商店支援的卡片類型 - 電子發票載具: MOBILE_CARRIER (功能預設不開啟) - 商家會員卡: {類別名稱需與LINE Pay洽談確認}
				CardId   string `json:"cardId"`   // 交易中若用戶符合商店支援的卡片類型所對應的內容值
			} `json:"affiliateCards"`
		} `json:"merchantReference"`
		// info.shipping為使用到Checkout時的資訊，目前Checkout只限定日本可以使用
		Shipping *struct {
			MethondId string `json:"methodId,omitempty"`  // 用戶所選的配送方式ID
			FeeAmount int    `json:"feeAmount,omitempty"` // 運費
			Address   *struct {
				Country    string `json:"country,omitempty"`    // 收貨國家
				PostalCode string `json:"postalCode,omitempty"` // 收貨地郵政編碼
				State      string `json:"state,omitempty"`      // 收貨地區
				City       string `json:"city,omitempty"`       // 收貨省市區
				Detail     string `json:"detail,omitempty"`     // 收貨地址
				Optional   string `json:"optional,omitempty"`   // 詳細地址資訊
				Recipient  *struct {
					FirstName         string `json:"firstName,omitempty"`         // 收貨人名
					LastName          string `json:"lastName,omitempty"`          // 收貨人姓
					FirstNameOptional string `json:"firstNameOptional,omitempty"` // 詳細名資訊
					LastNameOptional  string `json:"lastNameOptional,omitempty"`  // 詳細姓資訊
					Email             string `json:"email,omitempty"`             // 收貨人電子郵件
					PhoneNo           string `json:"phoneNo,omitempty"`           // 收貨人電話號碼
				} `json:"recipient,omitempty"`
			} `json:"address,omitempty"`
		} `json:"shipping,omitempty"`
	} `json:"info"`
}

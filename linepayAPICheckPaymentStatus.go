package linepay

import (
	"context"
	"fmt"
	"net/http"
)

// CheckPaymentStatus API
// 本API查詢LINE Pay付款請求的狀態。商家應隔一段時間後直接檢查付款狀態，不透過confirmUrl查看用戶是否已經確認付款，最終判斷交易是否完成。
// 建議將Check Payment Status API的請求週期設置為1秒

func (c *Client) CheckPaymentStatus(ctx context.Context, transactionID int64, req *CheckPaymentStatusRequest) (*CheckPaymentStatusResponse, *http.Response, error) {
	path := fmt.Sprintf("/v3/payments/requests/%d/check", transactionID)
	httpReq, err := c.NewRequest(http.MethodGet, path, req)
	if err != nil {
		return nil, nil, err
	}
	resp := new(CheckPaymentStatusResponse)
	httpResp, err := c.Do(ctx, httpReq, resp)
	if err != nil {
		return nil, httpResp, err
	}
	return resp, httpResp, nil
}

// CheckPaymentStatusRequest type
type CheckPaymentStatusRequest struct {
}

// CheckPaymentStatusResponse type
type CheckPaymentStatusResponse struct {
	ReturnCode    string `json:"returnCode"`    // 結果代碼
	ReturnMessage string `json:"returnMessage"` // 結果訊息或失敗原因
	// info.shipping為使用到Checkout時的資訊，目前Checkout只限定日本可以使用
	Info struct {
		Shipping struct {
			MethondId string `json:"methodId,omitempty"`  // 用戶所選的配送方式ID
			FeeAmount int    `json:"feeAmount,omitempty"` // 運費
		}
	} `json:"info,omitempty"`
}
